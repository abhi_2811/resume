
$(function(){

    Chart.pluginService.register({
		beforeDraw: function (chart) {
			if (chart.config.options.elements.center) {
        //Get ctx from string
        var ctx = chart.chart.ctx;
        
				//Get options from the center object in options
        var centerConfig = chart.config.options.elements.center;
      	var fontStyle = centerConfig.fontStyle || 'Arial';
				var txt = centerConfig.text;
        var color = centerConfig.color || '#000';
        var sidePadding = centerConfig.sidePadding || 20;
        var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
        //Start with a base font of 30px
        ctx.font = "40px " + fontStyle;
        
				//Get the width of the string and also the width of the element minus 10 to give it 5px side padding
        var stringWidth = ctx.measureText(txt).width;
        var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

        // Find out how much the font can grow in width.
        var widthRatio = elementWidth / stringWidth;
        var newFontSize = Math.floor(30 * widthRatio);
        var elementHeight = (chart.innerRadius * 2);

        // Pick a new font size so it will not be larger than the height of label.
        var fontSizeToUse = Math.min(newFontSize, elementHeight);

				//Set font settings to draw it correctly.
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
        var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
        ctx.font = fontSizeToUse+"px " + fontStyle;
        ctx.fillStyle = color;
        
        //Draw text in center
        ctx.fillText(txt, centerX, centerY);
			}
		}
	});

    //get the doughnut chart canvas
    var ctx1 = $("#doughnut-chartcanvas-1");
    var ctx2 = $("#doughnut-chartcanvas-2");
    var ctx3 = $("#doughnut-chartcanvas-3");
    var ctx4 = $("#doughnut-chartcanvas-4");
    var ctx5 = $("#doughnut-chartcanvas-5");
    var ctx6 = $("#doughnut-chartcanvas-6");

    //doughnut chart data
    var data1 = {
        datasets: [
            {
                label: "ML",
                data: [20,80],
                backgroundColor: [
                    "#DCDCDC",
                    "#9ACD32"
                ],
                borderColor: [
                    "#CBCBCB",
                    "#89BC21"
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data2 = {
        datasets: [
            {
                label: "APIs",
                data: [40,60],
                backgroundColor: [
                    "#DCDCDC",
                    "#9ACD32"
                ],
                borderColor: [
                    "#CBCBCB",
                    "#89BC21"
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    var data3 = {
        datasets: [
            {   
                label: "Backend",
                data: [40,60],
                backgroundColor: [
                    "#DCDCDC",
                    "#9ACD32"
                ],
                borderColor: [
                    "#CBCBCB",
                    "#89BC21"
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    var data4 = {
        datasets: [
            {
                label: "python",
                data: [25,75],
                backgroundColor: [
                    "#DCDCDC",
                    "#4AB9AE"
                ],
                borderColor: [
                    "#CBCBCB",
                    "#45ABA0"
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //doughnut chart data
    var data5 = {
        datasets: [
            {
                label: "java",
                data: [50,50],
                backgroundColor: [
                    "#DCDCDC",
                    "#4AB9AE"
                ],
                borderColor: [
                    "#CBCBCB",
                    "#45ABA0"
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    var data6 = {
        datasets: [
            {   
                label: "C++",
                data: [68,32],
                backgroundColor: [
                    "#DCDCDC",
                    "#4AB9AE"
                ],
                borderColor: [
                    "#CBCBCB",
                    "#45ABA0"
                ],
                borderWidth: [1, 1]
            }
        ]
    };

    //options
    var options1 = {
        responsive: true,
        elements: {
            center: {
                text: 'ML',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20, // Defualt is 20 (as a percentage)
                innerRadius: "70%"
            }
        },
        cutoutPercentage: 80
    };

    var options2 = {
        responsive: true,
        elements: {
            center: {
                text: 'API',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20 // Defualt is 20 (as a percentage)
            }
        },
        cutoutPercentage: 80
    };

    var options3 = {
        responsive: true,
        elements: {
            center: {
                text: 'Backend',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20 // Defualt is 20 (as a percentage)
            }
        },
        cutoutPercentage: 80
    };

    var options4 = {
        responsive: true,
        elements: {
            center: {
                text: 'Python',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20 // Defualt is 20 (as a percentage)
            }
        },
        cutoutPercentage: 80
    };
    var options5 = {
        responsive: true,
        elements: {
            center: {
                text: 'Java',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20 // Defualt is 20 (as a percentage)
            }
        },
        cutoutPercentage: 80
    };

    var options6 = {
        responsive: true,
        elements: {
            center: {
                text: 'C++',
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20 // Defualt is 20 (as a percentage)
            }
        },
        cutoutPercentage: 80
    };

    //create Chart class object
    var chart1 = new Chart(ctx1, {
        type: "doughnut",
        data: data1,
        options: options1
    });


    var chart2 = new Chart(ctx2, {
        type: "doughnut",
        data: data2,
        options: options2
    });

    var chart3 = new Chart(ctx3, {
        type: "doughnut",
        data: data3,
        options: options3
    });

    var chart4 = new Chart(ctx4, {
        type: "doughnut",
        data: data4,
        options: options4
    });

    var chart5 = new Chart(ctx5, {
        type: "doughnut",
        data: data5,
        options: options5
    });

    var chart6 = new Chart(ctx6, {
        type: "doughnut",
        data: data6,
        options: options6
    });

});